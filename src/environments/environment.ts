// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDgBtCN9iA0XWfZ_B5zA3FBdKBtlUQbLek',
    authDomain: 'ainhoa-alpha.firebaseapp.com',
    databaseURL: 'https://ainhoa-alpha.firebaseio.com',
    projectId: 'ainhoa-alpha',
    storageBucket: 'ainhoa-alpha.appspot.com',
    messagingSenderId: '72877767119'
  }
};
