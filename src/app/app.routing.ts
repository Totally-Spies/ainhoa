import {LoginComponent} from './pages/login/login.component';
import {RegisterComponent} from './pages/register/register.component';
import {ForgotPasswordComponent} from './pages/forgot-password/forgot-password.component';
import {MainComponent} from './templates/main/main.component';
import {Routes} from '@angular/router';
import {AuthGuard} from './guards/auth.guard';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {AccountComponent} from './pages/account/account.component';
import {HomeComponent} from './pages/home/home.component';
import {DocumentationComponent} from './pages/documentation/documentation.component';
import {MessagesComponent} from './pages/messages/messages.component';
import {NotConnectedComponent} from './templates/not-connected/not-connected.component';
import {CoachSubmitionPageComponent} from './pages/coach-submition-page/coach-submition-page.component';

export const appRoutes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: 'account', component: AccountComponent},
      {path: 'account/coach-subscription', component: CoachSubmitionPageComponent},
      {path: 'messages', component: MessagesComponent},
      {path: 'documentation', component: DocumentationComponent},
      {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
    ],
  },
  {
    path: 'home',
    component: NotConnectedComponent,
    children: [
      {path: '', component: HomeComponent},
      {path: 'login', component: LoginComponent},
      {path: 'register', component: RegisterComponent},
      {path: 'reset-password', component: ForgotPasswordComponent},
      {path: '', redirectTo: '/home', pathMatch: 'full'},
    ]
  },
  {path: '**', component: PageNotFoundComponent}
];
