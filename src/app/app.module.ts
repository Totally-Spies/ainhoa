import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {MainModule} from './templates/main/main.module';
import {environment} from '../environments/environment';
import {AuthGuard} from './guards/auth.guard';
import {AngularFireModule} from 'angularfire2';
import {appRoutes} from './app.routing';
import {FormsModule} from '@angular/forms';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {ThemeService} from './services/theme.service';
import {NotConnectedModule} from './templates/not-connected/not-connected.module';
import { CoachSubmitionStepComponent } from './organisms/coach-submition/forms/coach-submition-step/coach-submition-step.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    CoachSubmitionStepComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    SweetAlert2Module.forRoot(),
    FormsModule,
    MainModule,
    NotConnectedModule
  ],
  providers: [AuthGuard, ThemeService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
