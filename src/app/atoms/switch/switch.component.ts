import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss']
})
export class SwitchComponent {

  @Input()
  public checked = false;
  @Input()
  public label: string;

  @Output()
  public onToggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  public emit(): void {
    this.onToggle.emit(this.checked);
  }

}
