import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoachButtonComponent} from './coach-button.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CoachButtonComponent],
  exports: [CoachButtonComponent]
})
export class CoachButtonModule { }
