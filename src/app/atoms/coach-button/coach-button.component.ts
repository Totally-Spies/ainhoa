import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-coach-button',
  templateUrl: './coach-button.component.html',
  styleUrls: ['./coach-button.component.scss']
})
export class CoachButtonComponent {

  @Output()
  public onCLick: EventEmitter<any> = new EventEmitter();

}
