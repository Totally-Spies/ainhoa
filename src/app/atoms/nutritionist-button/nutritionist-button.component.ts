import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-nutritionist-button',
  templateUrl: './nutritionist-button.component.html',
  styleUrls: ['./nutritionist-button.component.scss']
})
export class NutritionistButtonComponent {
  @Output()
  public onCLick: EventEmitter<any> = new EventEmitter();
}
