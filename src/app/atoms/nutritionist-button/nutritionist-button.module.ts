import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NutritionistButtonComponent} from './nutritionist-button.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [NutritionistButtonComponent],
  exports: [NutritionistButtonComponent]
})
export class NutritionistButtonModule { }
