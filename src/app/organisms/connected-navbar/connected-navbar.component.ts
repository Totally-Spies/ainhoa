import {Component, ElementRef, ViewChild} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-connected-navbar',
  templateUrl: './connected-navbar.component.html',
  styleUrls: ['./connected-navbar.component.scss']
})
export class ConnectedNavbarComponent {

  @ViewChild('toggleMenu')
  public toggleMenuEl: ElementRef;

  @ViewChild('navbarMenu')
  public navbarMenuEl: ElementRef;

  public routesList: RouteItem[] = [
    {label: 'Home', icon: 'fa-home', route: '/dashboard'},
    {label: 'Documentation', icon: 'fa-book-open', route: '/documentation', customClass: 'has-text-danger'},
    {label: 'Account', icon: 'fa-user', route: '/account', customClass: 'has-text-success'},
  ];

  constructor(public authSrv: AuthService, public  router: Router) {
  }

  public logout(): void {
    this.authSrv.logout().then(() => this.router.navigate(['/home']));
  }

  public toggle(): void {
    this.toggleMenuEl.nativeElement.classList.toggle('is-active');
    this.navbarMenuEl.nativeElement.classList.toggle('is-active');
  }

}

class RouteItem {
  label: string;
  icon: string;
  route: string;
  customClass?: string;
}
