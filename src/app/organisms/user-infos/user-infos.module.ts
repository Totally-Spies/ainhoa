import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EditAdvancedInfoComponent} from '../edit-advanced-info/edit-advanced-info.component';
import {EditPersonalInfoComponent} from '../edit-personal-info/edit-personal-info.component';
import {UserInfosComponent} from './user-infos.component';
import { SettingsComponent } from '../settings/settings.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [EditAdvancedInfoComponent, EditPersonalInfoComponent, UserInfosComponent, SettingsComponent],
  exports: [UserInfosComponent]
})
export class UserInfosModule { }
