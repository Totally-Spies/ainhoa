import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-user-infos',
  templateUrl: './user-infos.component.html',
  styleUrls: ['./user-infos.component.scss']
})
export class UserInfosComponent implements OnInit {

  public isEditingPersonalInfo = false;
  public isEditingAdvancedInfo = false;

  constructor() {
  }

  ngOnInit() {
  }

  public toggleEditPersonalInfo(): void {
    this.isEditingPersonalInfo = !this.isEditingPersonalInfo;
  }

  public toggleEditAdvancedInfo(): void {
    this.isEditingAdvancedInfo = !this.isEditingAdvancedInfo;
  }

}
