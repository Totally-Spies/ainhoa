import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-edit-personal-info',
  templateUrl: './edit-personal-info.component.html',
  styleUrls: ['./edit-personal-info.component.scss']
})
export class EditPersonalInfoComponent {

  @Input()
  isEditing = false;

  constructor() {
  }

  public toggleEdit(): void {
    this.isEditing = !this.isEditing;
  }

}
