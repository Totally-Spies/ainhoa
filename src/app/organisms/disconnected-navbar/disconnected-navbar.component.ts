import {Component} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-disconnected-navbar',
  templateUrl: './disconnected-navbar.component.html',
  styleUrls: ['./disconnected-navbar.component.scss']
})
export class DisconnectedNavbarComponent {

}
