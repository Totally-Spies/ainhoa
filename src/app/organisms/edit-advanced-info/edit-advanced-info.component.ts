import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-edit-advanced-info',
  templateUrl: './edit-advanced-info.component.html',
  styleUrls: ['./edit-advanced-info.component.scss']
})
export class EditAdvancedInfoComponent {

  @Input()
  public isEditing: boolean;

  constructor() { }

}
