import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent {

  @Input()
  public isOpen = true;

  @Output()
  public onSidenavToggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  public routesList: RouteItem[] = [
    {label: 'Overview', icon: 'far fa-sun', route: '/dashboard'},
    {label: 'Documentation', icon: 'far fa-futbol', route: '/documentation'},
    {label: 'Messages', icon: 'far fa-comment', route: '/messages'},
    {label: 'Account', icon: 'far fa-laugh', route: '/account'},
  ];
  public currentRoute: string;

  constructor(public router: Router) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.currentRoute = val.url;
      }
    });
  }
}

class RouteItem {
  label: string;
  icon: string;
  route: string;
}
