import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CoachSubmitionStepComponent} from '../coach-submition-step/coach-submition-step.component';

@Component({
  selector: 'app-coach-submition-step-one',
  templateUrl: './coach-submition-step-one.component.html',
  styleUrls: ['./coach-submition-step-one.component.scss']
})
export class CoachSubmitionStepOneComponent extends CoachSubmitionStepComponent implements OnInit {

  constructor(private fb: FormBuilder) {
    super();
    this.form = this.fb.group({
      professionalTitle: ['', Validators.required],
      description: ['', Validators.required],
    });
    this.onUpdate();
  }

  ngOnInit() {
  }

}
