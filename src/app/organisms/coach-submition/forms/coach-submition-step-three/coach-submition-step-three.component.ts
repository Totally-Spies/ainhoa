import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CoachSubmitionStepComponent} from '../coach-submition-step/coach-submition-step.component';

@Component({
  selector: 'app-coach-submition-step-three',
  templateUrl: './coach-submition-step-three.component.html',
  styleUrls: ['./coach-submition-step-three.component.scss']
})
export class CoachSubmitionStepThreeComponent extends CoachSubmitionStepComponent implements OnInit {

  constructor(private fb: FormBuilder) {
    super();
    this.form = this.fb.group({
      pricing: ['', Validators.required]
    });
    this.onUpdate();
  }

  ngOnInit() {
  }

}
