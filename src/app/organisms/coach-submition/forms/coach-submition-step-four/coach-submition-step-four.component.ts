import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CoachSubmitionStepComponent} from '../coach-submition-step/coach-submition-step.component';

@Component({
  selector: 'app-coach-submition-step-four',
  templateUrl: './coach-submition-step-four.component.html',
  styleUrls: ['./coach-submition-step-four.component.scss']
})
export class CoachSubmitionStepFourComponent extends CoachSubmitionStepComponent {

  public files: Set<File>;

  constructor(public fb: FormBuilder) {
    super();
    this.form = this.fb.group({
      files: ['', Validators.required]
    });
    this.files = new Set();
    this.onUpdate();
  }

  public onFileInputChange(event) {
    const files = event.target.files;
    for (const file of files) {
      this.files.add(file);
    }
    this.form.patchValue({files: this.files});
  }

  public removeFile(name: string): void {
    this.files.forEach(value => {
      if (value.name === name) {
        this.files.delete(value);
      }
    });
  }

}
