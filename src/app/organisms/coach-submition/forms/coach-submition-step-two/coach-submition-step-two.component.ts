import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CoachSubmitionStepComponent} from '../coach-submition-step/coach-submition-step.component';

@Component({
  selector: 'app-coach-submition-step-two',
  templateUrl: './coach-submition-step-two.component.html',
  styleUrls: ['./coach-submition-step-two.component.scss']
})
export class CoachSubmitionStepTwoComponent extends CoachSubmitionStepComponent implements OnInit {

  constructor(private fb: FormBuilder) {
    super();
    this.form = this.fb.group({
      skills: ['', Validators.required]
    });
    this.onUpdate();
  }

  ngOnInit() {
  }

  public updateSkills(event) {
    this.form.patchValue({skills: event.target.value});
  }

}
