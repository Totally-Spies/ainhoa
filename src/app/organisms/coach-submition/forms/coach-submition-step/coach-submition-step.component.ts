import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-coach-submition-step',
  templateUrl: './coach-submition-step.component.html',
  styleUrls: ['./coach-submition-step.component.scss']
})
export class CoachSubmitionStepComponent implements OnInit {

  public form: FormGroup;
  @Output()
  public formUpdate: EventEmitter<FormGroup> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  public onUpdate(): void {
    this.form.valueChanges.subscribe(() => {
      if (this.form.valid) {
        this.formUpdate.emit(this.form);
      }
    });
  }
}
