import {Injectable} from '@angular/core';

@Injectable()
export class ThemeService {

  private themeStorageKey: string;
  private currentTheme: string;

  public html: HTMLElement;

  constructor() {
    this.themeStorageKey = 'ainhoa-theme';
    this.html = document.getElementsByTagName('html')[0];
    window.localStorage.getItem(this.themeStorageKey)
      ? this.currentTheme = window.localStorage.getItem(this.themeStorageKey)
      : this.currentTheme = 'light-theme';
  }

  public getCurrentTheme(): string {
    return this.currentTheme;
  }

  public setDarkTheme(): void {
    const theme = 'dark-theme';
    this.setTheme(theme);
  }

  public setLightTheme(): void {
    const theme = 'light-theme';
    this.setTheme(theme);
  }

  private resetTheme(): void {
    this.html.classList.remove('dark-theme');
    this.html.classList.remove('light-theme');
  }

  public setTheme(theme: string) {
    this.resetTheme();
    this.currentTheme = theme;
    this.html.classList.add(theme);
    window.localStorage.setItem(this.themeStorageKey, theme);
  }

}
