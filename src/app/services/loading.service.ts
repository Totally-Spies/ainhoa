import {Injectable} from '@angular/core';

@Injectable()
export class LoadingService {

  private pageLoader: HTMLElement;

  constructor() {
    this.pageLoader = document.getElementById('page-loader');
  }

  public displayPageLoader(): void {
    if (!this.pageLoader.classList.contains('is-active')) {
      this.pageLoader.classList.add('is-active');
    }
  }

  public removePageLoader(): void {
    this.pageLoader.classList.remove('is-active');
  }

}
