import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoachSubmitionPageComponent} from './coach-submition-page.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {CoachSubmitionStepTwoComponent} from '../../organisms/coach-submition/forms/coach-submition-step-two/coach-submition-step-two.component';
import {CoachSubmitionStepThreeComponent} from '../../organisms/coach-submition/forms/coach-submition-step-three/coach-submition-step-three.component';
import {CoachSubmitionStepFourComponent} from '../../organisms/coach-submition/forms/coach-submition-step-four/coach-submition-step-four.component';
import {CoachSubmitionStepOneComponent} from '../../organisms/coach-submition/forms/coach-submition-step-one/coach-submition-step-one.component';
import {LoadingService} from '../../services/loading.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [
    CoachSubmitionPageComponent,
    CoachSubmitionStepOneComponent,
    CoachSubmitionStepTwoComponent,
    CoachSubmitionStepThreeComponent,
    CoachSubmitionStepFourComponent],
  exports: [CoachSubmitionPageComponent],
  providers: [LoadingService]
})
export class CoachSubmitionPageModule {
}
