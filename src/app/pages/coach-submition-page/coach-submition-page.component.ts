import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {LoadingService} from '../../services/loading.service';

declare const bulmaTagsinput, bulmaSteps;

@Component({
  selector: 'app-coach-submition-page',
  templateUrl: './coach-submition-page.component.html',
  styleUrls: ['./coach-submition-page.component.scss']
})
export class CoachSubmitionPageComponent implements OnInit {

  public firstForm: FormGroup;
  public secondForm: FormGroup;
  public thirdForm: FormGroup;
  public fourthForm: FormGroup;
  public stepperIndex: number;
  public termsAndCondition: boolean;
  public steps: string[] = ['Info', 'Skills', 'Pricing', 'Docs', 'Finish'];

  constructor(private loadingService: LoadingService) {
    this.stepperIndex = 0;
    this.termsAndCondition = false;

  }

  ngOnInit() {
    bulmaTagsinput.attach();
    bulmaSteps.attach();
  }

  public get canValidate(): boolean {
    if (!this.firstForm || !this.secondForm || !this.thirdForm || !this.fourthForm) {
      return false;
    }
    return this.firstForm.valid && this.secondForm.valid && this.thirdForm.valid && this.fourthForm.valid && this.termsAndCondition;
  }

  public next(): void {
    if (this.stepperIndex < 4) {
      this.stepperIndex++;
    }
  }

  public previous(): void {
    if (this.stepperIndex > 0) {
      this.stepperIndex--;
    }
  }

  public onFirstStepFormUpdate(form: FormGroup) {
    this.firstForm = form;
  }

  public onSecondStepFormUpdate(form: FormGroup) {
    this.secondForm = form;
  }

  public onThirdStepFormUpdate(form: FormGroup) {
    this.thirdForm = form;
  }

  public onFourthStepFormUpdate(form: FormGroup) {
    this.fourthForm = form;
  }

  public termsValidationChange() {
    this.termsAndCondition = !this.termsAndCondition;
  }

  public cancel(): void {
    window.history.back();
  }

  public submit(): void {
    const data = {
      professionalTitle: this.firstForm.get('professionalTitle').value,
      description: this.firstForm.get('description').value,
      skills: this.secondForm.get('skills').value,
      pricing: this.thirdForm.get('pricing').value,
      files: this.fourthForm.get('files').value,
      acceptTermsAndConditions: this.termsAndCondition
    };
    // this.loadingService.displayPageLoader();
  }
}
