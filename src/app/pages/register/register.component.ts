import {Component} from '@angular/core';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import * as Noty from 'noty';

@Component({
  selector: 'app-registration-form',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  registrationForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
    confirmPassword: ['', Validators.required],
  }, {
    validator: RegisterComponent.matchPassword // your validation method
  });

  constructor(private fb: FormBuilder, public authService: AuthService) {
  }

  static matchPassword(AC: AbstractControl) {
    const password = AC.get('password').value; // to get value in input tag
    const confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
    if (password !== confirmPassword) {
      AC.get('confirmPassword').setErrors({MatchPassword: true});
    } else {
      return null;
    }
  }

  onSubmit() {
    this.authService.signUpWithEmail(this.registrationForm.value.email, this.registrationForm.value.password).then(res => {
      new Noty({
        timeout: 3000,
        type: 'success',
        theme: 'semanticui',
        text: 'Your account has been correctly created'
      }).show();
    }).catch(err => {
      new Noty({
        timeout: 3000,
        type: 'error',
        theme: 'semanticui',
        text: 'Something bad happened :('
      }).show();
    });
  }

}
