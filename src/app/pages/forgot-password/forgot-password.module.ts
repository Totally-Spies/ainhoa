import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ForgotPasswordComponent} from './forgot-password.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [ForgotPasswordComponent],
  providers: [AngularFireAuth, AuthService],
  exports: [ForgotPasswordComponent]
})
export class ForgotPasswordModule {
}
