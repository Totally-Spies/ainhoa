import {Component} from '@angular/core';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import * as Noty from 'noty';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {

  resetPasswordForm = this.fb.group({
    email: ['', Validators.required],
  });

  constructor(private fb: FormBuilder, public authService: AuthService) {
  }

  onSubmit() {
    // todo
  }
}
