import {Component} from '@angular/core';
import {ThemeService} from '../../services/theme.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent {

  public darkModeActive = false;

  constructor(private themeSrv: ThemeService) {
    this.darkModeActive = (this.themeSrv.getCurrentTheme() === 'dark-theme');
  }

  public toggleTheme(event): void {
    event ? this.themeSrv.setDarkTheme() : this.themeSrv.setLightTheme();
  }

  public openCoachModal(): void {

  }
}
