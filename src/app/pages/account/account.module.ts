import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AccountComponent} from './account.component';
import {PosterComponent} from '../../molecules/poster/poster.component';
import {UserInfosModule} from '../../organisms/user-infos/user-infos.module';
import {CoachButtonModule} from '../../atoms/coach-button/coach-button.module';
import {NutritionistButtonModule} from '../../atoms/nutritionist-button/nutritionist-button.module';
import {RouterModule} from '@angular/router';
import {CoachSubmitionPageModule} from '../coach-submition-page/coach-submition-page.module';

@NgModule({
  imports: [
    CommonModule,
    UserInfosModule,
    CoachButtonModule,
    RouterModule,
    CoachSubmitionPageModule,
    NutritionistButtonModule
  ],
  declarations: [AccountComponent, PosterComponent],
  exports: [AccountComponent]
})
export class AccountModule {
}
