import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import * as Noty from 'noty';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public isLoading = false;

  loginForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(public authService: AuthService, private router: Router, private fb: FormBuilder) {
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.isLoading = true;
      this.authService.emailLogin(this.loginForm.value.email, this.loginForm.value.password)
        .then(() => {
          this.isLoading = false;
          this.router.navigate(['/']);
        }).catch((err) => {
        new Noty({
          timeout: 3000,
          type: 'success',
          theme: 'semanticui',
          text: err.message
        }).show();
        this.isLoading = false;
      });
    }
  }
}
