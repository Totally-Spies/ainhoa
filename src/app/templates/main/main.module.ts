import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {MainComponent} from './main.component';
import {AccountModule} from '../../pages/account/account.module';
import {DashboardModule} from '../../pages/dashboard/dashboard.module';
import {SideNavComponent} from '../../organisms/side-nav/side-nav.component';
import {DocumentationModule} from '../../pages/documentation/documentation.module';
import {MessagesComponent} from '../../pages/messages/messages.component';
import {ConnectedNavbarComponent} from '../../organisms/connected-navbar/connected-navbar.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AccountModule,
    DashboardModule,
    DocumentationModule,
    RouterModule
  ],
  declarations: [MainComponent, SideNavComponent, MessagesComponent, ConnectedNavbarComponent],
  exports: [MainComponent]
})
export class MainModule {
}
