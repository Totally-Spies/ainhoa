import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  isSidenavOpen = true;

  constructor() {
    this.isSidenavOpen = (localStorage.getItem('side-nav-open') === 'true');
  }

  ngOnInit() {
  }

  onSidenavToggle(event: boolean) {
    this.isSidenavOpen = event;
    localStorage.setItem('side-nav-open', event.toString());
  }
}
