import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-connected',
  templateUrl: './not-connected.component.html',
  styleUrls: ['./not-connected.component.scss']
})
export class NotConnectedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
