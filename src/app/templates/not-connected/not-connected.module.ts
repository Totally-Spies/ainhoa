import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotConnectedComponent } from './not-connected.component';
import {DisconnectedNavbarComponent} from '../../organisms/disconnected-navbar/disconnected-navbar.component';
import {RouterModule} from '@angular/router';
import {LoginModule} from '../../pages/login/login.module';
import {RegisterModule} from '../../pages/register/register.module';
import {ForgotPasswordModule} from '../../pages/forgot-password/forgot-password.module';
import {HomeModule} from '../../pages/home/home.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    LoginModule,
    RegisterModule,
    ForgotPasswordModule,
    HomeModule
  ],
  declarations: [NotConnectedComponent, DisconnectedNavbarComponent]
})
export class NotConnectedModule { }
